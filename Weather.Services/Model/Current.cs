﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Services.Model
{
    public class Current
    {
        
        public List<string> Weather_Descriptions { get; set; }
        public double Wind_Speed { get; set; }
        public double Uv_Index { get; set; }
       
    }
}
