﻿using System;
using System.Collections.Generic;
using System.Text;
using Weather.Services.Model;

namespace FTPWeatherChallenge
{
    class ConsoleWriter
    {

        public static void Output(ServiceResponse response)
        {
            if (response == null)
            {
                Console.WriteLine("Invalid zipcode");
                return;
            }

            Console.WriteLine("Should I go outside?");

            Console.WriteLine(response.GoOutSide ? "Yes" : "No");

            Console.WriteLine("Should I wear sunscreen?");

            Console.WriteLine(response.WearSunScreen ? "Yes" : "No");

            Console.WriteLine("Can I fly my kite?");

            Console.WriteLine(response.FlyKite ? "Yes" : "No");
        }

        public static string InputZipCode()
        {
            Console.WriteLine("Please input zipcode: ");
            string zipCode = Console.ReadLine();
            return zipCode;
        }
    }
}
